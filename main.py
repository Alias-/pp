from config import CHOICE, VALUE, score_bord
from random import choice

def player():
    player_choice = input("enter your choice : ")
    if player_choice not in CHOICE:
        print("ops!, please try again ")
        player()
    return player_choice


def system():
    system_choice = choice(CHOICE)
    print(system_choice)
    return system_choice


def final(play_, system_):

    match = {play_, system_}
    if len(match) == 1:
       return None

    return VALUE[tuple(sorted(match))]


def play():
    i=1
    while i <= 3 :
        player_choice = player()
        system_choice = system()
        win = final(player_choice, system_choice)
        if player_choice == win:
            score_bord["player"] += 1
        if system_choice == win:
            score_bord["system"] += 1
        if win == None:
            print("draw")
        i += 1

    print(score_bord['player'])
    print(score_bord['system'])


if __name__ == '__main__':
    play()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
